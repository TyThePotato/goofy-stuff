package com.tythepotato.zerrabot;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class Commands extends ListenerAdapter { 	
	
	public String[] listOfSwears;
	
	public final String imageNameStart = "https://raw.githubusercontent.com/TyThePotato/SwearBotStuff/master/Images/";
	
	public int maxImage;
	
	public void InitializeSwears () {
		try {
			String amountOfSwears = readStringFromURL("https://raw.githubusercontent.com/TyThePotato/SwearBotStuff/master/Images/SwearCount.txt");
			maxImage = Integer.parseInt(amountOfSwears.split("\\r?\\n")[0]);
			listOfSwears = readStringFromURL("https://raw.githubusercontent.com/TyThePotato/SwearBotStuff/master/swears.txt").split("\\r?\\n");
		} catch (IOException | NumberFormatException e) { // E
			e.printStackTrace();
		}
	}
	
	// When a message is recieved this function is called
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
		if(event.getAuthor().isBot()) return; // make sure a bot didnt send the message
		
		int SwearCount = 0;

		for(String swear : listOfSwears) {
			if(event.getMessage().getContentRaw().toLowerCase().contains(swear.toLowerCase())) {
				SwearCount++;
			}
		}
		
		if(SwearCount > 0) {
			InitializeSwears();
			Message msg = SendEmbedImage(event.getChannel(), imageNameStart + random(1,maxImage) + ".jpg");
			event.getMessage().delete().queue();
			msg.delete().queueAfter(10, TimeUnit.SECONDS);
		}
		
		try {
			Zucc(App.jarPath() + "zucc.txt",event.getGuild().getName() + " | " + event.getChannel().getName() +  " | " + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator() + " | : " + event.getMessage().getContentRaw());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	Message SendEmbedImage (MessageChannel channel, String url) {
		EmbedBuilder eb = new EmbedBuilder();
		eb.setImage(url);
		Message msg = channel.sendMessage(eb.build()).complete();
		return msg;
	}
	
	// returns if the string is an int
	boolean isInt(String str)
	{
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	int random (int min, int max) {
		Random r = new Random();
		int n = r.nextInt(max) + min;
		return n;
	}
	
	void Zucc (String filename, String data) throws IOException { //zuccs data
	    BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
	    writer.append(data);
	    writer.newLine();
	    writer.close();
	}
	
	public static String readStringFromURL(String requestURL) throws IOException
	{
	    try (Scanner scanner = new Scanner(new URL(requestURL).openStream(),
	            StandardCharsets.UTF_8.toString()))
	    {
	        scanner.useDelimiter("\\A");
	        return scanner.hasNext() ? scanner.next() : "";
	    }
	}
	
}
