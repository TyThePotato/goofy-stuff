package com.tythepotato.zerrabot;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Game.GameType;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/*
 * TODO:
 * - If settings json doesn't exist, create it and then inform the user to fill it in
 * - If settings json *does* exist, parse it and then return a string list of the values?
 */

public class App extends ListenerAdapter {
	
	public static JDA api;
	
	public static BotSettings bs = new BotSettings();
	
    public static void main( String[] args ) throws Exception {
    	
    	File settingsFile = new File(jarPath() + "config.json");
    	
    	if(settingsFile.exists()) {
    		
    		String json = new String(Files.readAllBytes(Paths.get(jarPath() + "config.json")));
    		Gson gson = new Gson();
    		bs = gson.fromJson(json, BotSettings.class);
    		
	        api = new JDABuilder(AccountType.BOT).setToken(bs.token).buildBlocking(); // this is the big boy
	        api.getPresence().setGame(Game.of(GameType.LISTENING, "closely for swears"));
	        
	        Commands cmds = new Commands();
	        
	        cmds.InitializeSwears();
	        
	        api.addEventListener(cmds); // listener
    	} else {
    		System.out.println("Config JSON not found! Creating...");
    		
    		String jsonTemplate;
    		Gson gson = new GsonBuilder().setPrettyPrinting().create();
    		jsonTemplate = gson.toJson(bs);
    		Files.write(Paths.get(jarPath() + "config.json"), jsonTemplate.getBytes());
    		System.out.println("Created config file in the jar's directory. (" + jarPath() + "config.json" + ") Please set the values in the json before relaunching.");
    		
    		System.in.read();
    		System.exit(0);
    	}
        
    }
    
    static String jarPath () throws UnsupportedEncodingException {
    	String path = App.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    	String decodedPath = URLDecoder.decode(path, "UTF-8");
    	return decodedPath;
    }
}
