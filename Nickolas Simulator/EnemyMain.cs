using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMain : MonoBehaviour {

	public int health = 10;
	public int minDamage = 10;
	public int maxDamage = 20;

	public float accuracy = 0.075f; // 0 = perfect accuracy

	public AudioClip hitSound;
	public AudioClip sightSound;
	public AudioClip shootSound;
	public AudioClip dieSound;

	public int normalViewDistance = 5;
	public int alertViewDistance = 10;

	public LayerMask damageLayers;

	public float speed = 2;

	public string state = "idle"; 

	public Sprite[] walkingSprites;
	public Sprite[] dyingSprites;
	public Sprite[] shootingSprites;

	public Sprite idleSprite;

	public Object[] thingsToDestroy;

	private Sprite[] currentSprites;

	private GameObject[] pathPoints;

	private GameObject player;
	private CharacterController cc;

	private int animationIndex;

	private SpriteRenderer enemySprite;

	private bool playingAnimation;

	public bool dying;
	public bool dead;
	public bool shooting;
	public bool readyToShoot;

	private bool caughtPlayer;

	private int viewDistance;

	void Start () {
		player = GameObject.FindWithTag("Player");
		cc = gameObject.GetComponent<CharacterController>();

		pathPoints = GameObject.FindGameObjectsWithTag("PathPoint");
		enemySprite = gameObject.GetComponent<SpriteRenderer>();

		InvokeRepeating("TryShoot", 3, 1f);
	}

	void Update () {

		if(dead) return;
		
		if(canSeePlayer() && (state == "idle" || state == "alert")) state = "chase";

		if(health<1 && !dying) Die();

		switch(state) {
			
			case "idle":
				viewDistance = normalViewDistance;
				if(playingAnimation) StopAnimation();
				break;
			
			case "alert":
				viewDistance = alertViewDistance;
				if(playingAnimation) StopAnimation();
				break;

			case "chase":
				viewDistance = alertViewDistance;
				PlaySoundOnce(sightSound);
				currentSprites = walkingSprites;
				StartAnimation();
				ChasePlayer();
				break;

			case "shoot":
				StopAnimation();
				if(!shooting) {
					GetComponent<SpriteBillboarder>().LookAtPlayer = true;
					enemySprite.sprite = shootingSprites[0];
					Invoke("Shoot", Random.Range(0.3f, 0.7f));
					shooting = true;
				} 
				break;

			case "dying":
				//CancelInvoke();
				currentSprites = dyingSprites;
				TriggerAnimation();
				PlaySoundOnce(dieSound);
				dying = true;
				break;

			case "dead":
				dead = true;
				foreach(Object obj in thingsToDestroy) Destroy(obj);
				GameInfo.TotalEnemies--;
				enemySprite.sprite = dyingSprites[dyingSprites.Length-1];
				Destroy(this);
				break;

		}
		
	}

	void TryShoot () {
		if(state == "chase" && canSeePlayer()) {
			int Chance = Random.Range(0,10);
			if(Chance > 3) {
				state = "shoot";
			}
		}
	}

	void Shoot () {
		enemySprite.sprite = shootingSprites[1];
		NickTESS.PlaySound(shootSound, transform.position);

		Vector3 aimOffset = new Vector3(0,0,Random.Range(-accuracy, accuracy));

		RaycastHit hit;
		if(Physics.Raycast(transform.position, -transform.forward + aimOffset, out hit, 50, damageLayers, QueryTriggerInteraction.Ignore)) {
			Vector3 hitPoint = new Vector3(hit.point.x,Random.Range(hit.point.y + 0.3f,hit.point.y - 0.2f),hit.point.z);
			if(hit.transform.tag != "Player") {
				GameObject bh = Instantiate(Resources.Load("BulletHole", typeof(GameObject)), hitPoint + (hit.normal * 0.001f), Quaternion.LookRotation(hit.normal)) as GameObject;
				//bh.transform.parent = NickTESS.GetClosestObject(bh, bh.transform.position, 0.25f).transform;
				bh.transform.parent = hit.transform;
			} else {
				hit.transform.gameObject.GetComponent<PlayerStatManager>().Hurt(Random.Range(minDamage, maxDamage));
			}
		}

		Invoke("StopShooting", 0.25f);
	}

	void StopShooting () {
		enemySprite.sprite = shootingSprites[0];
		Invoke("ContinueChase", 0.3f);
	}

	void ContinueChase () {
		GetComponent<SpriteBillboarder>().LookAtPlayer = false;
		shooting = false;
		state = "chase";
	}

	public void DamageEnemy (int amt) {
		health -= amt;
		NickTESS.PlaySound(hitSound, transform.position);
		if(state == "idle") state = "alert";
	}

	void Die () {
		StopAnimation();
		CancelInvoke();
		caughtPlayer = false;
		state = "dying";
	}

	// Returns true if the enemy can see the player.
	bool canSeePlayer () {
		bool withinDistance = false;
		bool clearPath = false;

		withinDistance = Vector3.Distance(transform.position, player.transform.position) <= viewDistance;
		if (!withinDistance) return false;

		RaycastHit hit;
		if(Physics.Linecast(transform.position, player.transform.position, out hit, ~0, QueryTriggerInteraction.Ignore)) {
			clearPath = hit.transform.CompareTag("Player");
		}

		return withinDistance && clearPath;
	}

	void StartAnimation () { // loop
		if(!playingAnimation && !dying) {
			InvokeRepeating("PlayAnimation", 0, 0.15f);
			playingAnimation = true;
		}
	}
	
	void TriggerAnimation () { // once
		if(!playingAnimation && !dying) {
			animationIndex = 0;
			InvokeRepeating("PlayAnimationOnce", 0, 0.1f);
			playingAnimation = true;
		}
	}

	void StopAnimation () { // end loop
		if(playingAnimation && !dying) {
			CancelInvoke("PlayAnimation");
			playingAnimation = false;
			enemySprite.sprite = idleSprite;
		}
	}

	void PlayAnimation () { // To stop loop, do CancelInvoke("PlayAnimation");
		if(animationIndex < currentSprites.Length) {
			enemySprite.sprite = currentSprites[animationIndex];
			animationIndex++;
		} else {
			animationIndex = 0;
			enemySprite.sprite = currentSprites[animationIndex];
			animationIndex++;
			//if(!loop) CancelInvoke("PlayAnimation");
		}
	}

	void PlayAnimationOnce () {
		if(animationIndex < currentSprites.Length) {
			enemySprite.sprite = currentSprites[animationIndex];
			animationIndex++;
		} else {
			state = "dead";
			CancelInvoke("PlayAnimationOnce");
		}
	}

	
	void ChasePlayer () {
		GameObject nextTarget = determineNextTarget();
		Vector3 lookPos = new Vector3(nextTarget.transform.position.x, transform.position.y, nextTarget.transform.position.z);
		transform.LookAt(lookPos);
		cc.SimpleMove(transform.forward * speed);
	}
	
	GameObject determineNextTarget () {
		float playerDistance = Vector3.Distance(player.transform.position, transform.position);

		for(int i = 0; i < pathPoints.Length; i++) {
			float pathPointDistance = Vector3.Distance(pathPoints[i].transform.position, transform.position);
			if(pathPointDistance < playerDistance) { // Pathpoint is closer
				float pathPointToPlayerDistance = Vector3.Distance(player.transform.position, pathPoints[i].transform.position);
				if(pathPointToPlayerDistance < playerDistance) return pathPoints[i];
			} else {
				return player;
			}
		}

		return player;
	}

	void PlaySoundOnce (AudioClip ac) {
		if(caughtPlayer) return;
		NickTESS.PlaySound(ac, transform.position);
		caughtPlayer = true;
	}
	

}
