﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;

public class Subscribe : ModuleBase<SocketCommandContext>
{
    [Command("subscribe")]
    [Summary("Subscribes to Screenshot Saturday.")]
    public async Task subscribeAsync()
    {
        var user = Context.User; //Holds the user for quick access

        var everybodyRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "everybody"); //Everybody role
        var subscribedRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Subscribed"); //Subscribed role
        var unsubscribedRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Unsubscribed"); //Unsubscribed role

        if (!GuilduserExtensions.HasRole(user as IGuildUser, unsubscribedRole)) //Have they not unsubcribed?
        {
            if (!GuilduserExtensions.HasRole(user as IGuildUser, subscribedRole)) //Are they not already subscribed?
            {
                if (GuilduserExtensions.HasRole(user as IGuildUser, everybodyRole)) //Do they have the everybody role?
                {
                    await (user as IGuildUser).RemoveRoleAsync(everybodyRole); //Remove the everybody role
                }

                await (user as IGuildUser).AddRoleAsync(subscribedRole); //Add the subscribed role
                await Context.Channel.SendMessageAsync($"{everybodyRole.Mention} {user.Mention} has joined the Screenshot Saturday! :smile:"); //Send message
                Console.WriteLine($"{user.Username} has subscribed to screenshot saturday."); //Debug
            }
        }


    }

}

public class Unsubscribe : ModuleBase<SocketCommandContext>
{
    [Command("unsubscribe")]
    [Summary("Unsubscribes from Screenshot Saturday")]
    public async Task unsubscribeAsync()
    {
        var user = Context.User; //Holds the user for quick access

        var everybodyRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "everybody"); //Everybody role
        var subscribedRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Subscribed"); //Subscribed role
        var unsubscribedRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Unsubscribed"); //Unsubscribed role

        if (!GuilduserExtensions.HasRole(user as IGuildUser, unsubscribedRole)) //Are they not already unsubscribed?
        {
            if (GuilduserExtensions.HasRole(user as IGuildUser, subscribedRole)) //Are they instead subscribed?
            {
                if (GuilduserExtensions.HasRole(user as IGuildUser, everybodyRole)) //Do they have everybody role
                {
                    await (user as IGuildUser).RemoveRoleAsync(everybodyRole); //Remove it
                }

                await (user as IGuildUser).RemoveRoleAsync(subscribedRole); //Remove subscribed role
                await (user as IGuildUser).AddRoleAsync(unsubscribedRole); //Add unsubscribed role
                Console.WriteLine($"{user.Username} has unsubscribed from screenshot saturday."); //Debug
            }
        }
    }
}

public class FixRole : ModuleBase<SocketCommandContext>
{
    [Command("fixrole")]
    [Summary("Removes 'everybody' role if the user still has it after subscribing.")]
    public async Task fixroleAsync()
    {

        var user = Context.User; //Holds the user for quick access

        var everybodyRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "everybody"); //Everybody role
        var subscribedRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Subscribed"); //Subscribed role
        var unsubscribedRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Unsubscribed"); //Unsubscribed role

        if (GuilduserExtensions.HasRole(user as IGuildUser, subscribedRole)) //Do they have the subscribed role?
        {
            if (GuilduserExtensions.HasRole(user as IGuildUser, everybodyRole)) //Do they also have the everybody role?
            {
                await (user as IGuildUser).RemoveRoleAsync(everybodyRole); //Removes everybody role
                Console.WriteLine($"{user.Username} has fixed their role."); //Debug
            }
        }
    }
}

public class BroadcastAnnouncement : ModuleBase<SocketCommandContext>
{
    [Command("broadcast")]
    [Summary("Broadcasts an announcement to all users in the guild.")]
    public async Task broadcastAnnouncementAsync([Summary("Message to broadcast")] String extraData = null)
    {
        var founderRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Founder"); //Holds the founder role
        if (GuilduserExtensions.HasRole(Context.User as IGuildUser, founderRole)) //Does the user have founder?
        {
            IReadOnlyCollection<IGuildUser> users = Context.Guild.Users; //List of users
            foreach (var user in users) //Cycles through each user
            {

                if (extraData != null) //Did we add a message at the end?
                {
                    if (!user.IsBot) //Are they not a bot?
                    {
                        Console.WriteLine($"Sent message to: {user}, they are currently {user.Status}."); //Debug
                        await user.SendMessageAsync(extraData); //Sends message
                        Thread.Sleep(500); //Pauses for .2 seconds
                    }
                } else //If we did not add a message
                {
                    break; //Stops the for loop from running
                }
            }
        }
    }
}

public class DebugCommands : ModuleBase<SocketCommandContext>
{
    [Command("time")]
    [Summary("Returns current time.")]
    public async Task timeCommandAsync()
    {
        var founderRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Founder");

        if (GuilduserExtensions.HasRole(Context.User as IGuildUser, founderRole))
        {
            await Context.User.SendMessageAsync(System.DateTime.Now.DayOfWeek.ToString());
        }
    }

    [Command("lmd")]
    [Summary("Returns date of last screenshot saturday reminder")]
    public async Task lmdAsync()
    {

        var founderRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Founder");

        if (GuilduserExtensions.HasRole(Context.User as IGuildUser, founderRole))
        {

            var generalChannel = Context.Guild.GetChannel(337259370494820360) as IMessageChannel;

            var msg = await generalChannel.GetMessageAsync(Convert.ToUInt64(System.IO.File.ReadAllText("bot/id.txt"))); // o.O

            var eb = new EmbedBuilder();

            eb.WithTitle("Last SS Reminder Info");
            eb.WithDescription($"Sent at: {msg.Timestamp}\nId: {msg.Id}");
            eb.WithColor(Color.Red);

            await Context.User.SendMessageAsync("", false, eb);
        }
    }

    [Command("rolechart")]
    [Summary("Makes a chart of what roles each user has")]
    public async Task rolechartAsync()
    {

        var founderRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Founder");

        if (GuilduserExtensions.HasRole(Context.User as IGuildUser, founderRole))
        {
            var everybodyRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "everybody");
            var subRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Subscribed");
            var unsubRole = Context.Guild.Roles.FirstOrDefault(x => x.Name == "Unsubscribed");
            int userCount = Context.Guild.MemberCount;
            int ebCount = everybodyRole.Members.Count();
            int subCount = subRole.Members.Count();
            int unsubCount = unsubRole.Members.Count();

            await Context.User.SendMessageAsync($"Role : Users\nAll : {userCount}\nEverybody : {ebCount}\nSubscribed : {subCount}\nUnsubscribed : {unsubCount}");
        }
    }
}

public static class GuilduserExtensions
{
    public static bool HasRole(this IGuildUser user, IRole role)
    {
        if (user.RoleIds.Any(x => x == role.Id))
            return true;
        else
            return false;

    }
}