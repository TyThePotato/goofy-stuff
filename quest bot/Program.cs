﻿using System;
using System.Threading.Tasks;
using System.Reflection;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Timers;
using System.Text;

namespace NewQuestBot
{
    public class Program
    {

        private CommandService _commands;
        private DiscordSocketClient _client;
        private IServiceProvider _services;

        private ulong serverId;
        private ulong generalChannelId;

        static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            _client = new DiscordSocketClient();
            _commands = new CommandService();

            serverId = 301456405985230848;
            generalChannelId = 337259370494820360; //if changed, change in commandmodules too

            var thisServer = _client.GetGuild(serverId);

            //Variable of the token
            string token = "haha get prank";

            //Services
            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();

            _client.Log += Log;
            _client.UserJoined += UserJoined;

            await InstallCommandsAsync();

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();
            await _client.SetGameAsync("Questoria Shall Prevail!");

            await Task.Run(() => RunTimer());

            await Task.Delay(-1);
        }

        public async Task InstallCommandsAsync()
        {
            // Hook the MessageReceived Event into our Command Handler
            _client.MessageReceived += HandleCommandAsync;
            
            // Discover all of the commands in this assembly and load them.
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a System Message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;
            
            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;
            
            // Determine if the message is a command, based on if it starts with '!' or a mention prefix
            if (!(message.HasCharPrefix('!', ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))) return;
            
            // Create a Command Context
            var context = new SocketCommandContext(_client, message);
            
            // Execute the command. (result does not indicate a return value, 
            // rather an object stating if the command executed successfully)
            var result = await _commands.ExecuteAsync(context, argPos, _services);
        }


        private async Task UserJoined(IGuildUser user)
        {
            var everybodyRole = user.Guild.Roles.FirstOrDefault(x => x.Name == "everybody");
            await user.AddRoleAsync(everybodyRole);
        }

        private void RunTimer()
        {
            Timer msgTimer = new Timer(14400000);

            msgTimer.Elapsed += new ElapsedEventHandler(alarmMessage);
            msgTimer.Interval = 14400000;
            msgTimer.Enabled = true;
        }

        private async void alarmMessage(object source, ElapsedEventArgs e) 
        {

            if (System.DateTime.Now.DayOfWeek.ToString() == "Saturday")
            {

                Console.WriteLine("Attempting SS Message...");

                IMessageChannel curChannel;

                var thisServer = _client.GetGuild(serverId);
                var everybodyRole = thisServer.Roles.FirstOrDefault(x => x.Name == "everybody");
                var timermessagetosend = $"{everybodyRole.Mention} If you haven't already signed up for Screenshot Saturday then please use the command `!subscribe` to join in on the fun!\n- If you're already subscribed but keep getting pinged then just use the command `!fixrole`.";

                curChannel = thisServer.GetChannel(generalChannelId) as IMessageChannel;

                if (System.IO.File.ReadAllText("bot/id.txt") != "not")
                {
                    var msg = await curChannel.GetMessageAsync(Convert.ToUInt64(System.IO.File.ReadAllText("bot/id.txt")));
                    await msg.DeleteAsync();
                }

                var newReminder = await curChannel.SendMessageAsync(timermessagetosend);
                System.IO.File.WriteAllText("bot/id.txt", newReminder.Id.ToString());
            }

        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
