using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;

public class Furniture : MonoBehaviour
{

    public float DestroyTime;
    public float activationDistance = 2.65f;

    public Material BrokenMaterial;
    public Mesh BrokenMesh; // This variable can be left empty if the mesh doesnt change for some reason
    public AudioClip breakSound;

    private float DestroyProgress;

    private GameManager gm;
	private Renderer rnd;
    private GameObject player;
    private PlayerMovement playerMovement;
	private ParticleSystem ps;

    private bool insideTrigger;
    private bool destroying;

    void Start()
    {
        gm = GameManager.instance;
        player = gm.player;
        playerMovement = player.GetComponent<PlayerMovement>();
		rnd = GetComponent<Renderer>();

       rnd.material = Instantiate(rnd.material); // creates new material because yes

        if (transform.CompareTag("Furniture")) transform.tag = "Furniture";
    }

    void Update()
    {
        Vector3 offset = player.transform.position - transform.position;
        float sqrLength = offset.sqrMagnitude;
        bool triggerStatus = sqrLength < activationDistance * activationDistance;
        SetTriggerStatus(triggerStatus);

        if (insideTrigger && EllieInputManager.IsActivated() && !destroying) { AttemptToDestroy(); }
    }


    private void AttemptToDestroy()
    {
        StartCoroutine(RepeatingDamage(DestroyTime));
        InvokeRepeating("BitingSounds", 0, 0.65f);
        destroying = true;
        playerMovement.playerState = Utils.PlayerState.Eating;

        ps = Utils.EatingParticles(player.transform, new Vector3(0, 0, 1.5f), rnd.material.mainTexture as Texture2D);
        GameObject psContainer = new GameObject("EatingParticlesContainer");
        ps.transform.parent = psContainer.transform;
        psContainer.transform.parent = transform;
    }

    private void BitingSounds()
    {
        int index = Random.Range(0, gm.bitingSounds.Length);
        Utils.PlaySound(gm.bitingSounds[index]);
    }

    private void ActuallyDestroy()
    {
        bool changeMesh = BrokenMesh != null;

        CancelInvoke("BitingSounds");

       	rnd.material = BrokenMaterial;
        if (changeMesh) GetComponent<MeshFilter>().mesh = BrokenMesh;

        gm.DestroyFurniture(gameObject);
        Utils.PlaySound(breakSound);
        playerMovement.playerState = Utils.PlayerState.Idle;

        gm.RuinRing.fillAmount = 0;

        ps.Stop();
        Destroy(this);
        Debug.Log(gm.RuinRing.fillAmount);
    }

    private void EnterTrigger()
    {
        // enter
    }

    private void ExitTrigger()
    {
        // exit
        StopAllCoroutines();
        CancelInvoke("BitingSounds");

        gm.RuinRing.fillAmount = 0;

        destroying = false;
        playerMovement.playerState = Utils.PlayerState.Idle;

        if (ps)
        {
            ps.Stop();
            Destroy(ps.transform.parent.gameObject, 0.5f);
        }
    }

    // My possibly shoddy recreation of Trigger Collider's Enter and Exit events
    private void SetTriggerStatus(bool status)
    {
        if (insideTrigger == status) return;
        insideTrigger = status;

        Debug.Log("Trigger Event! Name:" + gameObject.name + ", State:" + status);

        if (status)
        {
            EnterTrigger();
        }
        else
        {
            ExitTrigger();
        }
    }

    private IEnumerator RepeatingDamage(float t)
    {
        while (DestroyProgress < 1.0f)
        {
            DestroyProgress = DestroyProgress + (Time.deltaTime / t);
            Damage(DestroyProgress);
            gm.RuinRing.fillAmount = DestroyProgress;
            yield return null;
        }
    }

    private void Damage(float val)
    {
        val = Mathf.Clamp01(val);

        // crack
        Material sh = rnd.material;
        sh.SetFloat("_DetailFillPercent", val);

        // other crap
        if (val >= 1) ActuallyDestroy();
    }
}
